# -*- coding: utf-8 -*-
"""
Created on Tue Nov 03 14:09:59 2015

@author: Filip
"""

#Unos broja i provjera je li ispravno upisan
try:
    broj = input('Unesite jedan broj izmedu 0.0 i 1.0: ') 
except: 
    print 'Broj nije upisan'
    exit()
    

#Provjera uvjeta i ispisivanje kategorije
if (broj >= 0.9 and broj <= 1.0):
    print 'A'
elif (broj >= 0.8 and broj <= 1.0):
    print 'B'
elif (broj >= 0.7 and broj <= 1.0):
    print 'C'
elif (broj >= 0.6 and broj <= 1.0):
    print 'D'
elif (broj < 0.6 and broj >= 0):
    print 'F'
else: 
    print 'Broj je izvan intervala 0.0 i 1.0'
